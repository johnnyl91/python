# 4. Draw this pretty pattern.

import turtle

def draw_square(t, sz):
    for i in range(4):
        t.forward(sz)
        t.left(90)

window = turtle.Screen()
window.bgcolor("lightgreen")
window.title("Pretty pattern")

tess = turtle.Turtle()
tess.color("blue")
tess.pensize(3)
# tess.penup()


for i in range(1):
    draw_square(tess, 200)
    tess.penup()
    tess.left(90)

window.mainloop()

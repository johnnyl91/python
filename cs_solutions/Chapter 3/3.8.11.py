# 11. Write a program to draw a shape like this: (classic star)

import turtle
wn = turtle.Screen()
sid = turtle.Turtle()

sid.hideturtle()
for i in range(6):
    sid.forward(300)
    sid.left(-144)       # Drawing equals to otal rotatin of 180 degrees. 180 // 5 (Points of star) = 36. 180 - 36 = 144.

wn.mainloop()

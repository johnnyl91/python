# 10. At the interactive prompt, anticipate what each of the following lines will do, and then record what happens. Score yourself, giving yourself one point for each one you anticipate correctly:

import turtle
wn = turtle.Screen()
tess = turtle.Turtle()

tess.right(90)        # tess will turn right and point to south.
tess.left(3600)       # tess will turn ten (10) complete left turns and point south.
tess.right(-90)       # tess will turn left and point to east.
tess.speed(10)        # tess speed will change to 10 (fastest speed with animation).
tess.left(3600)       # tess will turn ten (10) complete left turns really fast and point to east.
tess.speed(0)         # tess speed will change to 0 (fastest speed without animnation).
tess.left(3645)       # tess will turn ten (10) complete left turns and then 45 degrees pointing to north east.
tess.forward(-100)    # tess will move backwards 100 steps in soth west direction in speed 0.

wn.mainloop()
# 6. Use for loops to make a turtle draw these regular polygons (regular means all sides the same lengths, all angles the same):

import turtle
wn = turtle.Screen()
sid = turtle.Turtle()

# An equilateral triangle
for i in range(3):
    sid.forward(250)
    sid.left(120)

# A square
for i in range(4):
    sid.forward(250)
    sid.left(90)

# A hexagon (six sides)
for i in range(6):
    sid.forward(200)
    sid.left(60)

# An octagon (eight sides)
for i in range(8):
    sid.forward(80)
    sid.left(45)

wn.mainloop()
# 9. If you were going to draw a regular polygon with 18 sides, what angle would you need to turn the turtle at each corner?

print('If we draw a regular polygon with 18 sides we need to turn', 360 // 18, 'degrees at each corner')

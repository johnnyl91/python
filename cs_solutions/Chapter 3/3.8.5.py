# 5. Assume you have the assignment xs = [12, 10, 32, 3, 66, 17, 42, 99, 20]
xs = [12, 10, 32, 3, 66, 17, 42, 99, 20]

# Write a loop that prints each of the numbers on a new line.
for i in xs:
    print(i)

# Write a loop that prints each number and its square on a new line.
for i in xs:
    print([i])

# Write a loop that adds all the numbers from the list into a variable called total. You should set the total variable to have the value 0 before you start adding them up, and print the value in total after the loop has completed.
total = 0

for i in xs:
    total = total + i
print(total)

# Print the product of all the numbers in the list. (product means all multiplied together)
print(xs[0] * xs[1] * xs[2] * xs[3] * xs[4] * xs[5] * xs[6] * xs[7] * xs[8])
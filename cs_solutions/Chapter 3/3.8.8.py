# 8. Enhance your program above to also tell us what the drunk pirate’s heading is after he has finished stumbling around. (Assume he begins at heading 0).

drunk_turns = [160, -43, 270, -97, -43, 200, -940, 17, -86]

total_turns = 0

for i in drunk_turns:
    total_turns = total_turns + i
total_turns = total_turns % 360
print("Drunk pirate's heading is", total_turns, "after he has finished stumbling around (Assuming he begins at heading 0).")